import React from 'react';

import Axios from 'axios';

import Sound from 'react-sound';

class AppContainer extends React.Conponent {
    constructor(props) {
        super(props);
        this.client_id = '';
        this.state = {
            track : {stream_url : '', title : '', artwork_url : ''}
        };
    }

    //lifecycle method. Called once a component is loaded
    componentDidMount(){
        this.randomTrack();
    }

    render() {
        return (
            <div className="scotch_music">

            </div>
        );
    }

    randomTrack () {
        let _this = this;
        Axios.get('https://api.soundcloud.com/playlists/209262931?client_id=${this.client_id}')
            .then(function (response) {
                const trackLength = response.data.tracks.length;
                const randomNumber  = Math.floor( (Math.random() * trackLength) + 1);

                _this.setState({track : response.data.tracks[randomNumber]});
            })
            .catch(function (err) {
                console.log(err);
            })
    }
}

export default AppContainer;
